// gojq - Go implementation of jq
package main

import (
	"os"

	"gitlab.com/go-cmds/gojq/cli"
)

func main() {
	os.Exit(cli.Run())
}
